import java.util.Scanner;

public class Vacaciones{
 public static void main(String args[]){
 
 Scanner in = new Scanner(System.in);
 
 String nombre = "";
 int antiguedad = 0, departamento = 0;
 int dia1_1 = 6, dia1_2 = 14, dia1_3 = 20, dia2_1 = 7, dia2_2 = 15, dia2_3 = 22, dia3_1 = 10, dia3_2 = 20, dia3_3 = 30;
  
 
 System.out.println("*****************************************");
 System.out.println("*****************************************");
 System.out.println("*  Bienvenidos a The Coca-Cola Company  *");
 System.out.println("*****************************************");
 System.out.println("*****************************************");
 System.out.println("Ingresar Nombre: ");
 nombre = in.nextLine();
 System.out.println("Hola Sr/a. " + nombre);
 System.out.println("Ingresar Antiguedad(a�os): ");
 antiguedad = in.nextInt(); 
 System.out.println("Ingresar Departamento al que pertenece: ");
 departamento = in.nextInt();
 

  if(departamento == 1){
    
    if(antiguedad == 1){
     System.out.println("El Sr/a. " + nombre + " tiene derecho a " + dia1_1 + " d�as de vacaciones");   
    } else if(antiguedad >= 2 && antiguedad <= 6){
     System.out.println("El Sr/a. " + nombre + " tiene derecho a " + dia1_2 + " d�as de vacaciones");  
    } else if(antiguedad >= 7){
     System.out.println("El Sr/a. " + nombre + " tiene derecho a " + dia1_3 + " d�as de vacaciones");
    }
   } 

      else if(departamento == 2){
       if(antiguedad == 1){
       System.out.println("El Sr/a. " + nombre + " tiene derecho a " + dia2_1 + " d�as de vacaciones");   
    }  else if(antiguedad >= 2 && antiguedad <= 6){
       System.out.println("El Sr/a. " + nombre + " tiene derecho a " + dia2_2 + " d�as de vacaciones");  
    }  else if(antiguedad >= 7){
       System.out.println("El Sr/a. " + nombre + " tiene derecho a " + dia2_3 + " d�as de vacaciones");
    } 
  }

     else if(departamento == 3){
       if(antiguedad == 1){
       System.out.println("El Sr/a. " + nombre + " tiene derecho a " + dia3_1 + " d�as de vacaciones");   
    }  else if(antiguedad >= 2 && antiguedad <= 6){
       System.out.println("El Sr/a. " + nombre + " tiene derecho a " + dia3_2 + " d�as de vacaciones");  
    }  else if(antiguedad >= 7){
       System.out.println("El Sr/a. " + nombre + " tiene derecho a " + dia3_3 + " d�as de vacaciones");
    }
   }
     
    else {
    
      System.out.println("El n�mero de departamento no est� disponible");
   }
 }
}