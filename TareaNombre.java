import java.util.Scanner;

public class TareaNombre{
 public static void main(String args[]){
  
  Scanner in = new Scanner(System.in);
  
  String nameuno = "";
  String namedos = "";
  String repetir = "";
 
 do{
    System.out.println("****************************************************");
    System.out.println("****************************************************");
    System.out.println("**Bienvenidos al sistema de comparaci�n de nombres**");
    System.out.println("****************************************************");
    System.out.println("****************************************************");

    System.out.println("�Cu�l es el nombre a comparar?");
   nameuno = in.nextLine();

    System.out.println("�Con cu�l nombre debemos comparar?");
   namedos = in.nextLine();

    if(nameuno.equals(namedos)){ 
  
    System.out.println("Los nombres indicados coinciden.");
    System.out.println("Desea repetir el proceso? (Y/N)");
     repetir = in.nextLine();
  
    } else {
     System.out.println("Los nombres indicados no coinciden.");
     System.out.println("Desea repetir el proceso? (Y/N)");
     repetir = in.nextLine();
   } 
  } while(repetir.equals("Y") || repetir.equals("y"));
 }
}
