import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FormularioTrece extends JFrame implements ActionListener{

	private JMenuBar menubar;
	private JMenu menu1;
	private JMenuItem item1, item2, item3;


	public FormularioTrece(){
		setLayout(null);
		menubar = new JMenuBar();//crear menubar
		setJMenuBar(menubar);

		menu1 = new JMenu("Colores");
		menubar.add(menu1);//Menu1 se agrega a menubar

		item1 = new JMenuItem("Rojo");
		item1.addActionListener(this);
		menu1.add(item1);

		item2 = new JMenuItem("Verde");
		item2.addActionListener(this);
		menu1.add(item2);

		item3 = new JMenuItem("Azul");
		item3.addActionListener(this);
		menu1.add(item3);
	}

	public void actionPerformed(ActionEvent e){

		Container fondo = this.getContentPane();//contenedor para el color

		if (e.getSource() == item1) {
			fondo.setBackground(new Color(255,0,0));
			
		}
		if (e.getSource() == item2) {
			fondo.setBackground(new Color(0,255,0));
		}
		if (e.getSource() == item3) {
			fondo.setBackground(new Color(0,0,255));
		}
	}

	public static void main(String args[]){

	FormularioTrece formulario1 = new FormularioTrece();
	formulario1.setBounds(0,0,400,300);
	formulario1.setVisible(true);
	formulario1.setLocationRelativeTo(null);
	}
}