import javax.swing.*;
import java.awt.event.*;

public class FormularioTest extends JFrame implements ActionListener{

	private JTextField textfield1;
	private JLabel label1;
	private JButton boton1;
	private JTextArea textarea1;

	public FormularioTest(){//constructor
		setLayout(null);
		label1 = new JLabel("Ingresar los datos");
		label1.setBounds(10,10,100,30);
		add(label1);

		boton1 = new JButton("Ingresar");
		boton1.setBounds(10,80,100,30);
		add(boton1);
		boton1.addActionListener(this);

		textfield1 = new JTextField();
		textfield1.setBounds(120,17,150,20);
		add(textfield1);

		textarea1 = new JTextArea();
		textarea1.setBounds(10,50,400,300);
		add(textarea1);

	}

	public void actionPerformed(ActionEvent e){
		
		for (int i = 1; i <= 3  ; i++) {
			if (e.getSource() == boton1) {
			String texto = textfield1.getText();
			textarea1.setText(texto);
		}	
	  }
	}
 	
 	public static void main(String args[]){

 		FormularioTest formulario1 = new FormularioTest();
 		formulario1.setBounds(0,0,350,200);
 		formulario1.setVisible(true);
 		formulario1.setResizable(false);
 		formulario1.setLocationRelativeTo(null);
 }
}