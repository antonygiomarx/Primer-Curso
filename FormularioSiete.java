import javax.swing.*;

public class FormularioSiete extends JFrame{

	private JTextField textfield1;
	private JTextArea textarea1;

	public FormularioSiete(){
		setLayout(null);
		textfield1 = new JTextField();
		textfield1.setBounds(10,10,200,30);
		add(textfield1);

		textarea1 = new JTextArea();
		textarea1.setBounds(10,50,400,300);
		add(textarea1);
	}

	public static void main(String args[]){

		FormularioSiete formulario1 = new FormularioSiete();
		formulario1.setBounds(0,0,540,400); //Coordenadas
		formulario1.setVisible(true);//Hacer que sea visible el formulario
		formulario1.setResizable(false);//Que el usuario no edite
		formulario1.setLocationRelativeTo(null);//Que el cuadro aparezca en medio
	}

}